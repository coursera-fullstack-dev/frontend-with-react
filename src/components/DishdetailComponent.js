/* eslint-disable react/jsx-pascal-case */
import React, { Component } from 'react';
import { Loading } from './LoadingComponent';
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  Breadcrumb,
  BreadcrumbItem,
  Button,
  Modal,
  ModalBody,
  ModalHeader,
  Col,
  Label,
  Row,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { Control, LocalForm, Errors } from 'react-redux-form';
import { baseUrl } from '../shared/baseUrl';
import { FadeTransform, Fade, Stagger } from 'react-animation-components';

const required = val => val && val.length;
const maxLength = len => val => !val || val.length <= len;
const minLength = len => val => !val || val.length >= len;

class CommentForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
    };
    this.toggleModal = this.toggleModal.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  toggleModal() {
    this.setState({
      isModalOpen: !this.state.isModalOpen,
    });
  }

  handleSubmit(values) {
    this.toggleModal();
    this.props.postComment(this.props.dishId, values.rating, values.author, values.comment);
  }

  render() {
    return (
      <>
        <div>
          <Button outline color="secondary" onClick={this.toggleModal}>
            <span className="fa fa-pencil" /> Submit Comment
          </Button>
        </div>
        <div>
          <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
            <ModalHeader>Submit Comment</ModalHeader>
            <ModalBody>
              <LocalForm onSubmit={values => this.handleSubmit(values)}>
                <Row className="form-group">
                  <Label htmlFor="rating" sm={12}>
                    Rating
                  </Label>
                  <Col sm={12}>
                    <Control.select
                      model=".rating"
                      className="form-control"
                      id="rating"
                      name="rating"
                    >
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                    </Control.select>
                  </Col>
                </Row>
                <Row className="form-group">
                  <Label htmlFor="authon" sm={12}>
                    Your Name
                  </Label>
                  <Col sm={12}>
                    <Control.text
                      model=".authon"
                      id="authon"
                      name="authon"
                      placeholder="Your Name"
                      className="form-control"
                      validators={{
                        required,
                        minLength: minLength(3),
                        maxLength: maxLength(15),
                      }}
                    />
                    <Errors
                      className="text-danger"
                      model=".authon"
                      show="touched"
                      messages={{
                        required: 'Required',
                        minLength: 'Must be greater than 2 characters',
                        maxLength: 'Must be 15 characters or less',
                      }}
                    />
                  </Col>
                </Row>
                <Row className="form-group">
                  <Label htmlFor="comment" sm={12}>
                    Comment
                  </Label>
                  <Col sm={12}>
                    <Control.textarea
                      className="form-control"
                      model=".comment"
                      rows="6"
                      id="comment"
                      name="comment"
                      validators={{
                        required,
                      }}
                    />
                    <Errors
                      className="text-danger"
                      model=".comment"
                      show="touched"
                      messages={{
                        required: 'Required',
                      }}
                    />
                  </Col>
                </Row>
                <Row className="form-group">
                  <Col md={{ size: 10 }}>
                    <Button type="submit" color="primary">
                      Submit
                    </Button>
                  </Col>
                </Row>
              </LocalForm>
            </ModalBody>
          </Modal>
        </div>
      </>
    );
  }
}

function RenderDish({ dish }) {
  return (
    <div>
      <FadeTransform in transformProps={{ exitTransform: 'scale(0.5) translateY(-50%' }}>
        <Card>
          <CardImg width="100%" src={baseUrl + dish.image} alt={dish.name}></CardImg>
          <CardBody>
            <CardTitle>{dish.name}</CardTitle>
            <CardText>{dish.description}</CardText>
          </CardBody>
        </Card>
      </FadeTransform>
    </div>
  );
}

function RenderComments({ comments, postComment, dishId }) {
  if (comments) {
    let options = { year: 'numeric', month: 'short', day: 'numeric' };
    const dishComments = comments.map(comment => {
      return (
        <Fade in>
          <div key={comment.id}>
            <p>{comment.comment}</p>
            <p>
              -- {comment.author},{' '}
              <span>{new Date(comment.date).toLocaleDateString('en-US', options)}</span>
            </p>
          </div>
        </Fade>
      );
    });
    return (
      <>
        <div>
          <h4>Comments</h4>
          <Stagger in>{dishComments}</Stagger>
        </div>
        <div>
          <CommentForm dishId={dishId} postComment={postComment} />
        </div>
      </>
    );
  } else {
    return <div> No Comments</div>;
  }
}

const Dishdetail = props => {
  if (props.isLoading) {
    return (
      <div className="container">
        <div className="row">
          <Loading />
        </div>
      </div>
    );
  } else if (props.errMess) {
    return (
      <div className="container">
        <div className="row">
          <h4>{props.errMess}</h4>
        </div>
      </div>
    );
  } else if (props.dish != null) {
    return (
      <div className="container">
        <div className="row">
          <Breadcrumb>
            <BreadcrumbItem>
              <Link to="/menu">Menu</Link>
            </BreadcrumbItem>
            <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
          </Breadcrumb>
          <div className="col-12">
            <h3>{props.dish.name}</h3>
            <hr />
          </div>
        </div>
        <div className="row">
          <div className="col-12 col-md-5 m-1">
            <RenderDish dish={props.dish} />
          </div>
          <div className="col-12 col-md-5 m-1">
            <RenderComments
              comments={props.comments}
              postComment={props.postComment}
              dishId={props.dish.id}
            />
          </div>
        </div>
      </div>
    );
  } else {
    return <div> no dish found</div>;
  }
};

export default Dishdetail;
