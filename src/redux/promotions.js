import * as ActionTypes from './actionTypes';

export const Promotions = (
  state = {
    isLoading: true,
    errMess: null,
    promotions: [],
  },
  action,
) => {
  switch (action.type) {
    case ActionTypes.ADD_PROMOS:
      return { ...state, isLoading: false, errMess: null, promotions: action.payload };
    case ActionTypes.PROMOS_FAILED:
      return { ...state, errMess: action.payload, isLoading: false, promotions: [] };
    case ActionTypes.PROMOS_LOADING:
      return { ...state, isLoading: true, errMess: null, promotions: [] };
    default:
      return state;
  }
};
